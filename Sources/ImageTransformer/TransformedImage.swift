import Foundation

public struct TransformedImage {
    public let thumbUrl: URL
    public let mediumUrl: URL
    public let largeUrl: URL
}
