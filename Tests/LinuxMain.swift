import XCTest

import ImageTransformerTests

var tests = [XCTestCaseEntry]()
tests += ImageTransformerTests.allTests()
XCTMain(tests)
